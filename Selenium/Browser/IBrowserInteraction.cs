﻿using OpenQA.Selenium;

namespace Selenium.Browser
{
    public interface IBrowserInteraction
    {
        void ClickOn(By reference);
        bool ExistsInBrowser(By reference);
        void RemoveTextFrom(By reference);
        string GetTextFrom(By reference);
        void GoTo(string url);
        

    }
}