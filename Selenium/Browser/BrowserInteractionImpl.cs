﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using NLog;
using OpenQA.Selenium;
using ILogger = Selenium.Logger.ILogger;

namespace Selenium.Browser
{
    public class BrowserInteractionImpl : IBrowserInteraction
    {
        private IWebDriver WebDriver { get; set; }
        private bool Debug { get; set; }
        private readonly ILogger _report;


        public BrowserInteractionImpl(IWebDriver webDriver, ILogger report)
        {
            WebDriver = webDriver;
            _report = report;
        }

        public void ClickOn(By reference)
        {
            _report.Debug("Click on " + reference);
            try
            {
                WebDriver.FindElement(reference).Click();
            }
            catch (Exception e)
            {
                _report.Error("Can not click on element, exception found", e,TakeScreenshot());
                throw;
            }
        }

        public bool ExistsInBrowser(By reference)
        {
            _report.Debug("Find out if " + reference + " exists");
            return WebDriver.FindElements(reference).Count != 0;
        }

        public void FillTextIn(By reference, string text)
        {
            _report.Debug("Fill " + text + " in " + reference);
            try
            {
                WebDriver.FindElement(reference).SendKeys(text);
            }
            catch (Exception e)
            {
                _report.Error("Can not fill text " + text + " on element, exception found", e,TakeScreenshot());
                _report.Debug("Screenshot taken ",TakeScreenshot());
                throw;
            }
        }

        public void RemoveTextFrom(By reference)
        {
            _report.Debug("Remove text from " + reference);
            try
            {
                WebDriver.FindElement(reference).Clear();
            }
            catch (Exception e)
            {
                _report.Error("Can not clear text from element, exception found", e,TakeScreenshot());
                _report.Debug("Screenshot taken ",TakeScreenshot());
                throw;
            }
        }


        public string GetTextFrom(By reference)
        {
            _report.Debug("Get Text from " + reference);
            try
            {
                return WebDriver.FindElement(reference).Text;
            }
            catch (Exception e)
            {
                _report.Error("Can not clear text from element, exception found", e,TakeScreenshot());
                _report.Debug("Screenshot taken ",TakeScreenshot());
                throw;
            }
        }

        public void GoTo(string url)
        {
            _report.Debug("Navigate to " + url);
            try
            {
                WebDriver.Navigate().GoToUrl(url);
            }
            catch (Exception e)
            {
                _report.Error("Can not navigate to " + url, e,TakeScreenshot());
                _report.Debug("Screenshot taken ",TakeScreenshot());
                throw;
            }
        }
        
        private string TakeScreenshot()
        {
            try
            {
                var fileNameBase = string.Format("screen_{0:yyyyMMdd_HHmmss}.png", DateTime.Now);
                if (!(WebDriver is ITakesScreenshot takesScreenshot)) return null;

                var screenshot = takesScreenshot.GetScreenshot();
                var screenshotFilePath = @"C:\Users\wluijk\RiderProjects\SWAT_Selenium\Selenium\bin\Debug" +
                                         fileNameBase;
                screenshot.SaveAsFile(screenshotFilePath, ScreenshotImageFormat.Png);

                return fileNameBase;
            }
            catch (Exception ex)
            {
                _report.Error("Error while taking screenshot: " + ex);
            }

            return null;
        }
    }
}