﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace Selenium.Browser
{
    public class BrowserFactory
    {
        public static IWebDriver GetBrowser()
        {
            const string desiredBrowserName = "firefox"; //TODO: Create Config Var

            //Room for more browsers
            switch (desiredBrowserName)
            {
                case "chrome":
                    return ChromeBrowser.BuildChromeBrowser();
                case "firefox":
                    var profileManager = new FirefoxProfileManager();
                    var profile = profileManager.GetProfile("Selenium");
                    var webdriver = new FirefoxDriver(profile);
                    webdriver.Manage().Window.Maximize();
                    return webdriver;
            }
        }
        
        private class ChromeBrowser : ChromeDriver
        {
            public static IWebDriver BuildChromeBrowser()
            {
                var options = new ChromeOptions();
                options.AddArguments("--start-maximized");
                return new ChromeBrowser(options);
            }

            private ChromeBrowser(ChromeOptions options) : base(@"C:\Users\wilfr\Downloads\chromedriver_win32 (1)", options)
            {
            }
        }
    }
}