﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;

namespace Selenium.Browser
{
    public class SharedDriver : EventFiringWebDriver
    {
        private static IWebDriver RealDriver { get; set; }

        private static void Run()
        {
            RealDriver.Close();
        }

        private static readonly Thread CloseThread = new Thread(Run);

        public SharedDriver(IWebDriver parentDriver) : base(parentDriver)
        {
        }

        public SharedDriver() : this(RealDriver)
        {
            RealDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }


        private static string OriginalHandle { get; set; }

        static SharedDriver()
        {
            AppDomain.CurrentDomain.ProcessExit += (a, b) => { CloseThread.Start(); };
            try
            {
                RealDriver = BrowserFactory.GetBrowser();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        public void Close()
        {
            if (Thread.CurrentThread != CloseThread)
            {
                throw new Exception();
            }

            base.Close();
        }


    }
}