﻿using NUnit.Framework;
using Selenium.Browser;
using Selenium.Logger;
using Selenium.Pages;

namespace Selenium.Test
{
    [TestFixture]
    public class Tests : BaseTest
    {
        private KzaMainPage _kzaMainPage;
        
        [SetUp]
        public void Before()
        {
            
            _kzaMainPage = new KzaMainPage(Browser);
        }
        
        [Test]
        public void Test1()
        {
            Logger.CreateTest("Test KZA homepage","Description");
            _kzaMainPage
                .OpenKzaPagina()
                .ClickContact();
        }
    }
}