﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using Selenium.Browser;
using Selenium.Logger;

namespace Selenium.Test
{
    [TestFixture]
    public class BaseTest
    {
        protected IWebDriver Driver { get; set; }
        protected IBrowserInteraction Browser { get; set; }
        protected ILogger Logger { get; set; }

     
        [SetUp]
        public void Init()
        {
            Driver = GlobalSetup.Driver;
            Browser = GlobalSetup.BrowserInteraction;
            Logger = GlobalSetup.Logger;
        }

        protected static int GetEpochInSeconds()
        {
            return (int) (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}