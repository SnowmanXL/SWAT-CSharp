﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using OpenQA.Selenium;
using Selenium.Browser;
using Selenium.Logger;

namespace Selenium.Test
{
    [SetUpFixture]
    public class GlobalSetup
    {
        public static IWebDriver Driver { get; set; }
        public static IBrowserInteraction BrowserInteraction { get; set; }
        public static ILogger Logger { get; set; }

        private ExtentHtmlReporter _htmlReporter;
        private ExtentReports _reports;

        [OneTimeSetUp]
        public void BaseInit()
        {
            //initiate shared driver
            Driver = new SharedDriver();

             // create ExtentReports and attach reporter(s) to logger 
            _htmlReporter = new ExtentHtmlReporter(TestContext.CurrentContext.TestDirectory + "\\Report.html");

            // finally, attach the reporter:
            _reports = new ExtentReports();
            _reports.AttachReporter(_htmlReporter);
            Logger = new ExtentLogger(_reports);

            BrowserInteraction = new BrowserInteractionImpl(Driver, Logger);
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            // if (!_isInitalized)
            _reports.Flush();
            // driver.Quit();
            // driver.Close();
        }
    }
}