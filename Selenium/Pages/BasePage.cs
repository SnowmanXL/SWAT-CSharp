﻿using Selenium.Browser;

namespace Selenium.Pages
{
    public abstract class BasePage
    {
        protected readonly IBrowserInteraction Browser;

        protected BasePage(IBrowserInteraction browserInteraction)
        {
            Browser = browserInteraction;
        }
    }
}