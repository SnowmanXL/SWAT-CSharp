﻿using OpenQA.Selenium;
using Selenium.Browser;

namespace Selenium.Pages
{
    public class KzaMainPage : BasePage
    {

        public KzaMainPage(IBrowserInteraction browserInteraction) : base(browserInteraction)
        {
        }
        
        private readonly By _contactButton = By.XPath("//a[@href='https://www.kza.nl/contact/']");
        private const string KzaUrl = "https://www.kza.nl";

        public KzaMainPage OpenKzaPagina()
        {
            Browser.GoTo(KzaUrl);
            return this;
        }
        
        public KzaContactPage ClickContact()
        {
            Browser.ClickOn(_contactButton);
            return new KzaContactPage(Browser);
        }
    }
}