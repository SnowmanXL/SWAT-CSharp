﻿using System;

namespace Selenium.Logger
{
    public interface ILogger
    {
        void CreateTest(string testname, string testdescription);

        void Info(string message, string screenshotpath = null);
        void Error(string message, Exception e = null, string screenshotpath = null);
        void Debug(string message, string screenshotpath = null);
        void Fail(string message);
        void Pass(string message);
        void Warning(string message);
    }
}