﻿using System;
using AventStack.ExtentReports;
using NLog;

namespace Selenium.Logger
{
    public class ExtentLogger : ILogger
    {
        private readonly ExtentReports _extentReports;
        private ExtentTest _test;
        private readonly NLog.Logger _logger = LogManager.GetCurrentClassLogger();

        public ExtentLogger(ExtentReports extentReports)
        {
            _extentReports = extentReports;
        }

        public void CreateTest(string testname, string testdescription)
        {
            _logger.Debug("Create test {} with description {}", testname, testdescription);
            _test = _extentReports.CreateTest(testname, testdescription);
        }

        public void Info(string message, string screenshotpath = null)
        {
            CreateDummyTestIfNull();

            if (screenshotpath == null)
            {
                _logger.Info(message);
                _test.Info(message);
                return;
            }
            _logger.Info("Message: {}, screenshotpath {}", message, screenshotpath);
            _test.Info(message, BuildScreenshot(screenshotpath));
        }

        public void Error(string message, Exception e = null, string screenshotpath = null)
        {
            CreateDummyTestIfNull();
            if (e != null && screenshotpath != null)
            {
                _logger.Error(message,e);
                _test.Error(e,BuildScreenshot(screenshotpath));
            } else if (e == null && screenshotpath != null)
            {
                _logger.Error(message);
                _test.Error(message, BuildScreenshot(screenshotpath));
            }
        }


        private void CreateDummyTestIfNull()
        {
            if (_test != null) return;
            _logger.Info("Testname has not been given, creating Nameless test");
            CreateTest("NamelessTest", "No description");
        }

        public void Debug(string message, string screenshotpath = null)
        {
            CreateDummyTestIfNull();

            if (screenshotpath == null)
            {
                _logger.Debug(message);
                _test.Debug(message);
                return;
            }

            var mediaModel = MediaEntityBuilder.CreateScreenCaptureFromPath("data:image/png;base64," + screenshotpath)
                .Build();
            _logger.Debug("Message: {}, screenshotpath {}", message, screenshotpath);
            _test.Debug(message, mediaModel);
        }

        public void Fail(string message)
        {
            CreateDummyTestIfNull();
            _logger.Warn("Test failed with message: {}", message);
            _test.Fail(message);
        }

        public void Pass(string message)
        {
            CreateDummyTestIfNull();
            _logger.Info("Test passed with message: {}", message);
            _test.Pass(message);
        }

        public void Warning(string message)
        {
            CreateDummyTestIfNull();
            _logger.Warn(message);
            _test.Warning(message);
        }


        private static MediaEntityModelProvider BuildScreenshot(string screenshotPath)
        {
            return MediaEntityBuilder.CreateScreenCaptureFromPath(screenshotPath).Build();
        }
    }
}